%dw 2.0
import * from dw::test::Asserts
---
payload must equalTo({
  "id": null,
  "items": [
    {
      "exception": null,
      "message": null,
      "payload": {
        "created": false,
        "success": true,
        "id": "a025g000002yxvvAAA",
        "errors": []
      },
      "id": "a025g000002yxvvAAA",
      "statusCode": null,
      "successful": true
    },
    {
      "exception": null,
      "message": null,
      "payload": {
        "created": false,
        "success": true,
        "id": "a025g000002yxw0AAA",
        "errors": []
      },
      "id": "a025g000002yxw0AAA",
      "statusCode": null,
      "successful": true
    },
    {
      "exception": null,
      "message": null,
      "payload": {
        "created": false,
        "success": true,
        "id": "a025g000002yxvuAAA",
        "errors": []
      },
      "id": "a025g000002yxvuAAA",
      "statusCode": null,
      "successful": true
    },
    {
      "exception": null,
      "message": null,
      "payload": {
        "created": false,
        "success": true,
        "id": "a025g000002yxw1AAA",
        "errors": []
      },
      "id": "a025g000002yxw1AAA",
      "statusCode": null,
      "successful": true
    },
    {
      "exception": null,
      "message": null,
      "payload": {
        "created": false,
        "success": true,
        "id": "a025g000002yxw2AAA",
        "errors": []
      },
      "id": "a025g000002yxw2AAA",
      "statusCode": null,
      "successful": true
    },
    {
      "exception": null,
      "message": null,
      "payload": {
        "created": false,
        "success": true,
        "id": "a025g000002yxvyAAA",
        "errors": []
      },
      "id": "a025g000002yxvyAAA",
      "statusCode": null,
      "successful": true
    },
    {
      "exception": null,
      "message": null,
      "payload": {
        "created": false,
        "success": true,
        "id": "a025g000002yxvxAAA",
        "errors": []
      },
      "id": "a025g000002yxvxAAA",
      "statusCode": null,
      "successful": true
    },
    {
      "exception": null,
      "message": null,
      "payload": {
        "created": false,
        "success": true,
        "id": "a025g000002yxvtAAA",
        "errors": []
      },
      "id": "a025g000002yxvtAAA",
      "statusCode": null,
      "successful": true
    },
    {
      "exception": null,
      "message": null,
      "payload": {
        "created": false,
        "success": true,
        "id": "a025g000002yxvwAAA",
        "errors": []
      },
      "id": "a025g000002yxvwAAA",
      "statusCode": null,
      "successful": true
    },
    {
      "exception": null,
      "message": null,
      "payload": {
        "created": false,
        "success": true,
        "id": "a025g000002yxvzAAA",
        "errors": []
      },
      "id": "a025g000002yxvzAAA",
      "statusCode": null,
      "successful": true
    }
  ],
  "successful": true
})