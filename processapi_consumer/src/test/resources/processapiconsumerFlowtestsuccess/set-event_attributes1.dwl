{
  "headers": {
    "user-agent": "PostmanRuntime/7.28.0",
    "accept": "*/*",
    "postman-token": "ce8461d7-daf5-40b8-9473-e0584312bb26",
    "host": "localhost:8082",
    "accept-encoding": "gzip, deflate, br",
    "connection": "keep-alive",
    "content-length": "0"
  },
  "clientCertificate": null,
  "method": "POST",
  "scheme": "http",
  "queryParams": {},
  "requestUri": "/consumer",
  "queryString": "",
  "version": "HTTP/1.1",
  "maskedRequestPath": null,
  "listenerPath": "/consumer",
  "relativePath": "/consumer",
  "localAddress": "/127.0.0.1:8082",
  "uriParams": {},
  "rawRequestUri": "/consumer",
  "rawRequestPath": "/consumer",
  "remoteAddress": "/127.0.0.1:58576",
  "requestPath": "/consumer"
}